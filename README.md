<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="logo.png" width="320" alt="Nest Logo" /></a>
</p>

## Musicstock-api installation instructions

```bash
$ npm install
```

## Running api

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test api

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

- Author - Wesley Franca

